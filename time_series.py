import numpy as np
import matplotlib.pyplot as plt
import scipy.fft
from scipy.optimize import curve_fit


def func_linear(x, coef_a, coef_b):
    return coef_a * x + coef_b


def make_series(a=1.00, nu=0.10, phi=0.00, alpha=100.00, beta=0.00, gamma=0.50, delta_t=1.00, n=230):
    """
    makes time series using given parameters:
    delta_t  -- constant step of time, sec
    n        -- number of measuring
    a        -- amplitude of harmonic component
    nu       -- frequency of harmonic component, Hertz
    phi      -- phase of harmonic component
    alpha    -- parameter of line-trend
    beta     -- parameter of line-trend
    gamma    -- Signal to Noise ratio
    """
    sigma_x = abs(a) / np.sqrt(2.0 * gamma)
    series = []
    t_arr = []
    for i in range(n):
        dzeta = np.random.normal(0.0, 1.0, 1)
        t = delta_t * i
        t_arr.append(t)
        series.append(float(alpha + beta * t + a * np.cos(2 * np.pi * nu * t - phi) + sigma_x * dzeta))
    return np.array(series), np.array(t_arr)


# example input data:
delta_t = 1.00   # -- constant step of time, sec
n = 230          # -- number of measuring
x_1 = 9.00       # -- critical value for_ separating noise and_ deterministic components
q = 0.01         # -- significance level
series, x_lab = make_series()


# 1. original time series
fig0, ax = plt.subplots()
ax.plot(x_lab, series)
ax.set_title('original time series')


# 2. exclude liner trend
#    approximation by liner function
popt, pcov = curve_fit(func_linear, x_lab, series, maxfev=10000)
plt.plot(x_lab, func_linear(x_lab, *popt), linestyle='dotted', color='tab:purple', label="linear trend")
ax.legend(loc='best')
plt.show()

fig1, ax = plt.subplots()
ax.set_title('time series without linear trend')
ax.plot(x_lab, series - func_linear(x_lab, *popt))


# 3. dispersion of series
disp = np.std(series)**2

plt.plot(x_lab, n*[np.std(series)], linestyle='dotted', color='tab:purple', label='standard deviation')
plt.plot(x_lab, n*[0], linestyle='dotted', color='tab:blue', label='mean')
plt.plot(x_lab, n*[- np.std(series)], linestyle='dotted', color='tab:purple')
ax.legend(loc='upper left')
plt.show()


# 4. Fast Fourier Transform
#    making periodogram
fig2, ax = plt.subplots()
ax.set_title('periodogram by Fast Fourier Transform')

ext_series = [*(series + n*[0.0 + 0.0j]), *(int(2**(round(np.log2(n))+2))-n)*[0.0 + 0.0j]]
ext_n = len(ext_series)
fft_series = scipy.fft.fft(ext_series)
periodogram = [(fft_series[i].real**2 + fft_series[i].imag**2)/(n**2) for i in range(ext_n)]
periodogram = periodogram[:ext_n//2]

nu_lab = [i/(ext_n * delta_t) for i in range(ext_n//2)]

ax.plot(nu_lab, periodogram)
ax.plot(nu_lab, len(nu_lab)*[disp*x_1/n], linestyle='dotted', color='tab:purple',
        label=f'noise level = {round(disp*x_1/n, 3)}')
ax.legend(loc='best')
plt.show()


# 5. making correlogram
#    parameters:
a_const = 0.25
n_ = n // 10

fig3, ax = plt.subplots()
ax.set_title('weighted correlogram')
correlogram = (np.fft.ifft(abs(fft_series)**2)).real / n
correlogram = correlogram[:ext_n//2]


# 6. weighted correlogram
w_correlogram = [correlogram[i] * (1 - 2*a_const + 2*a_const * np.cos(np.pi * i / n_)) for i in range(n_)]
ax.plot(x_lab[:n_], w_correlogram)
plt.show()


# 7. smooth Periodogram
fig4, ax = plt.subplots()
ax.set_title('smooth Periodogram')

len_c = len(w_correlogram)
w_correlogram_ext = [*(w_correlogram + len_c*[0.0 + 0.0j]), *(int(2**(round(np.log2(len_c))+2))-len_c)*[0.0 + 0.0j]]

smooth_periodogram = ((2.0*np.fft.fft(w_correlogram_ext).real) - w_correlogram_ext[0]) / n_
len_ext_c = len(w_correlogram_ext)
nu_lab = [i/(len_ext_c * delta_t) for i in range(len_ext_c//2)]

ax.plot(nu_lab, smooth_periodogram[:len(nu_lab)], label=f"N* = {round(n_,2)}, a = {round(a_const,2)}")
ax.legend(loc='best')
plt.show()
